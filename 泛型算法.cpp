#include <iostream>
#include <string>
#include <list>
#include <numeric>
#include <vector>
#include <iterator>
#include <fstream>
#include <algorithm>
using namespace std;
//int main(){
//	vector<int>vec;
//	for (int i = 0; i < 101; i++){
//		vec.push_back(i);
//	}
//	cout << accumulate(vec.cbegin(), vec.cend(), 0);
//	return 0;
//}
//int main() {
//	// 10.1
//	vector<int> ivec = { 1, 2, 3, 4, 5, 6, 6, 6, 2 };
//	cout << "ex 10.1: " << count(ivec.cbegin(), ivec.cend(), 6)
//		<< std::endl;
//
//	// 10.2
//	list<string> slst = { "aa", "aaa", "aa", "cc" };
//	cout << "ex 10.2: " <<count(slst.cbegin(), slst.cend(), "aa")
//		<< std::endl;
//
//	return 0;
//}
//int main() {
//	vector<double> v = { -9.21, 3.14 };
//
//	// 错误的调用方法
//	cout << std::accumulate(v.cbegin(), v.cend(), 0) << endl;
//	// 正确的调用方法
//	cout << accumulate(v.cbegin(), v.cend(), 0.0) <<endl;
//
//	return 0;
//}
//int main() {
//	vector<int> ivec{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
//	fill_n(ivec.begin(), ivec.size(), 0);
//
//	for (int i : ivec)
//		cout << i <<endl;
//
//	return 0;
//}
//int main() {
//	auto sum = [](int a, int b) { return a + b; };
//	cout << sum(1, 2) << endl;
//}


//void add(int a){
//	auto sum = [a](int b){return a + b; };
//	cout << sum(5) << endl;
//}
//int main(){
//	add(4);
//	add(5);
//}
int main(int argc, char *argv[]) {
	if (argc != 2) {
		cerr << "请给出文件名" << endl;
		return -1;
	}
	ifstream in(argv[1]);
	if (!in) {
		cerr << "无法打开输入文件" << endl;
		return -1;
	}

	// 创建流迭代器从文件读入字符串
	istream_iterator<string> in_iter(in);
	// 尾后迭代器
	istream_iterator<string> eof;
	vector<string> words;
	while (in_iter != eof)
		words.push_back(*in_iter++);    // 存入 vector 并递增迭代器

	for (auto word : words)
		cout << word << " ";
	cout << endl;

	return 0;
}