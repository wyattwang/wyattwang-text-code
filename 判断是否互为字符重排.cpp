#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

//给定两个字符串 s1 和 s2，请编写一个程序，确定其中一个字符串的字符重新排列后，能否变成另一个字符串
class Solution {
public:
	bool CheckPermutation(string s1, string s2) {
		vector<char>v1;
		vector<char>v2;
		for (int i = 0; i < s1.length(); i++)
		{
			v1.push_back(s1[i]);
		}
		for (int i = 0; i < s2.length(); i++)
		{
			v2.push_back(s2[i]);
		}
		sort(v1.begin(), v1.end());
		sort(v2.begin(), v2.end());
		if (v1.size() == v2.size())
		{
			for (int i = 0; i < v1.size(); i++)
			{
				if (v1[i] != v2[i])
					return false;
			}return true;
		}
		else return false;
	}
};
int main(){
	string s1 = "abc", s2 = "bca";
	Solution s;
	cout<<s.CheckPermutation(s1, s2);
}