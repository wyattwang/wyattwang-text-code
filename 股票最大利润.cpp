#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
using namespace std;
class Solution {
public:
	int maxProfit(vector<int>& prices) {
		if (prices.empty()) return 0;
		int p = INT_MAX;
		int g = INT_MIN;
		for (auto i : prices)
		{
			p = min(p, i);
			g = max(g, i - p);
		}
		return g;
	}
};
int main(){
	vector<int>vec{ 7, 1, 5, 3, 6, 4 };
	Solution s;

	cout <<s.maxProfit(vec) << endl;

}