#include <iostream>
#include <string>
#include <vector>
#include <iterator>
using namespace  std;

int main(){
	vector<int>vec{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	vector<int>::iterator begin = vec.begin(),end=vec.end();
	auto mid = begin + (end - begin) / 2;
	int target = 9;
	while (mid != end && *mid != target){
		if (*mid > target){
			end = mid;
		}
		else
		{
			begin = mid + 1;
			mid = begin + (end - begin) / 2;
		}
	}
	if (*mid == target)
		cout << "find:" << *mid << endl;
	else
	cout << "find nothing" << endl;
	return 0;
}