// 一次读入一行
#include <iostream>
#include <string>
using std::string;
using std::cin;
using std::cout;
using std::endl;

int main() {
	string line;
	while (getline(cin, line))
		cout << line << endl;

	return 0;
}

//一次读入一个单词
/*int main() {
	string word;
	while (cin >> word)
		cout << word << endl;

	return 0;
}
标准库string的输入运算符自动忽略开头的空白，从第一个有效字符开始计算，直到下一个空白字符。
Getline函数从给定的输入流读取数据直到遇到换行符为止。换行符也会被读取，但是不被保存。
3.4
大小
include <iostream>
#include <string>
using std::string;
using std::cin;
using std::cout;
using std::endl;

int main() {
string s1, s2;
cin >> s1 >> s2;
if (s1 == s2)
cout << "s1与s2相等" << endl;
else if (s1 > s2)
cout << "s1大于s2" << endl;
else
cout << "s1小于s2" << endl;

return 0;
}
长度
int main() {
string s1, s2;
cin >> s1 >> s2;
if (s1.length() == s2.length())
cout << "s1与s2长度相等" << endl;
else if (s1.length() > s2.length())
cout << "s1长于s2" << endl;
else
cout << "s1短于s2" << endl;

return 0;
}
*/