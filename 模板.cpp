#include <iostream>
using namespace std;
//类模板
template<class R>
class top {
public:
	top(R num1, R num2) :a(num1), b(num2){}
	R bigger() {
		return a > b ? a : b;
	}
private:
	R a;
	R b;
};
int main() {
	top<int> c(1, 5);
	cout << c.bigger() << endl;
	top<double>cc(1.5, 4.7);
	cout << cc.bigger() << endl;
}
//类的全特化
/*template<class R>
class top {
public:
	top(R num1, R num2) :a(num1), b(num2){}
	R bigger() {
		return a > b ? a : b;
	}
private:
	R a;
	R b;
};
template<> class top < double > {//模板特化
};
//类的偏特化

template<class R>
class top {
public:
	top(R num1, R num2) :a(num1), b(num2){}
	R bigger() {
		return a > b ? a : b;
	}
private:
	R a;
	R b;
};
template<class R>//偏特化
class top < R* > {
};
*/